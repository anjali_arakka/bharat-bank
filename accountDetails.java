

public class accountDetails {

    private String accountName ;
    private double balance ;
    private double interestRate  = 0.0;




    public double deposit(double addMoney) {
        balance += addMoney ;
        return balance ;
    }




    public double withdraw(double withdrawMoney) {

        double overdraftLimit = balance + (balance/5) ;

        if(accountName.equals("Current Account") && ((withdrawMoney > balance) && (withdrawMoney <= overdraftLimit))) {

            System.out.println( withdrawMoney + " Money has been withdrawn from Overdraft Limit...");
            balance = 0.0;

        }
        else if(withdrawMoney <= balance) {
            System.out.println( withdrawMoney + " Money has been withdrawn...");
            balance -= withdrawMoney;
        }
        else {
            System.out.println("You can't withdraw money!!!");
        }

        return balance ;
    }




    public accountDetails(String accountName, double balance) {
        this.accountName = accountName;
        this.balance = balance;

        if(accountName.equals("Savings Account")){
            interestRate = 3.0 ;
        }
    }




    public double calculateInterest() {
        double interest = (balance * interestRate * 0.25) / 100.0 ;
        balance += interest ;
        return interest ;

    }



    @Override
    public String toString() {
        return "\nAccount Name = " + accountName + "\nBalance = " + balance + "\nInterest Rate = " + interestRate ;
    }





    public static void main(String[] arg) {


        accountDetails gopal = new accountDetails("Current Account", 50000.0 ) ;
        System.out.println(gopal);


        gopal.deposit(10000.0);
        System.out.println(gopal.balance);


        double interest = gopal.calculateInterest();
        System.out.println("Quarterly Interest = " + interest);
        System.out.println(gopal.balance);

        gopal.withdraw(70000.0) ;
        System.out.println(gopal.balance);





        accountDetails amrita = new accountDetails("Savings Account", 100000.0) ;
        System.out.println(amrita);



        double interest1 = amrita.calculateInterest() ;
        System.out.println("Quarterly Interest = " + interest1);
        System.out.println(amrita.balance);



        amrita.withdraw(45000.0) ;
        System.out.println(amrita.balance);





    }

}
